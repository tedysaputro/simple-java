package com.nostra.iteration;

public interface NostraStack {
	
	public void push(int data);
	
	public int pop();
	
	public int max();
	
	public int size();
	
	public String print();

}
